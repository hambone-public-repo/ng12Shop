const express = require('express');
const server = express();

const port = process.env.port || '8888';

const inventoryRoute = require('./inventory.router');

server.use('/shop/api/', inventoryRoute);
server.listen(port);

console.log(`our super cool new Express server is running on port ${port}`);
