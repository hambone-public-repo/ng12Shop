const inventoryData = require('./inventory.data');

const express = require('express');
const router = express.Router();

router.route('/inventory').get(
    (request, response) => {
        response.json(inventoryData);
    }
);

module.exports = router;