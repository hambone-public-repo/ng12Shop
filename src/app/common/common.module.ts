import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NegativeFocusDirective } from './directives/negative-focus.directive';



@NgModule({
  declarations: [
    NegativeFocusDirective
  ],
  exports:[
    NegativeFocusDirective
  ],
  imports: [
    CommonModule
  ]
})
export class ShopCommonModule { }
