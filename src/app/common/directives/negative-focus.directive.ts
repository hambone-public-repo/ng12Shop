import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[ngshopNegativeFocus]'
})
export class NegativeFocusDirective {

  isSelected = false;

  constructor(private element: ElementRef) { }

  @HostListener('click', ['$event']) onClick($event: any) {
    this.isSelected = !this.isSelected;
    console.info('clicked: ' + this.isSelected);
    this.element.nativeElement.style.background = this.isSelected ? 'gray' : 'white';
    this.element.nativeElement.style.color =  this.isSelected ? 'white' : 'black';
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent): void {
     console.log('event', event);
     if(event.code === 'Enter' && this.isSelected) {
       const button = this.element.nativeElement.querySelector('.delete');
       button.click();
     }
  }

}
