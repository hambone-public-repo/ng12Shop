import { ElementRef } from '@angular/core';
import { NegativeFocusDirective } from './negative-focus.directive';

describe('NegativeFocusDirective', () => {
  it('should create an instance', () => {
    const directive = new NegativeFocusDirective({} as ElementRef);
    expect(directive).toBeTruthy();
  });
});
