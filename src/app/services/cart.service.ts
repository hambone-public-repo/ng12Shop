import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { Cart } from '../cart/models/cart.model'
import { InventoryItem } from '../inventory/models/inventory-item.model'
import {v4 as uuidv4} from 'uuid';

@Injectable()
export class CartService {

  private readonly cart: BehaviorSubject<Cart>

  constructor() { 

    const cart = localStorage.getItem('ourCart') !== null 
      ? JSON.parse(localStorage.getItem('ourCart') as any)
      : {
        numberOfItems: 0,
        total: 0,
        items: []
      }

    this.cart = new BehaviorSubject<Cart>(cart)

  }

  get cart$(): Observable<Cart> {
    return this.cart.asObservable()
  }

  addToCart(item: InventoryItem): void {

    const oldCart = this.cart.getValue();

    const newItem: InventoryItem = {
      ...item,
      cartItemUuid: uuidv4()
    }

    const newItems = [
      ...oldCart.items,
      newItem
    ]

    const newCart: Cart = {
      numberOfItems: newItems.length,
      total: oldCart.total + item.itemCost,
      items: newItems
    }

    this.cart.next(newCart)
    localStorage.setItem('ourCart', JSON.stringify(newCart))

  }

  removeFromCart(item: InventoryItem): void {

    const oldCart = this.cart.getValue();

    const newItems = oldCart.items.filter(x => x.cartItemUuid !== item.cartItemUuid);

    const newCart:Cart = {
      numberOfItems: newItems.length,
      total: oldCart.total - item.itemCost,
      items: newItems
    };

    this.cart.next(newCart);

    localStorage.setItem('ourCart', JSON.stringify(newCart));

  }

}
