import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { InventoryItem } from '../inventory/models/inventory-item.model';

@Injectable()
export class InventoryService {

  private readonly inventory: BehaviorSubject<InventoryItem[]>;

  constructor(private http: HttpClient) {
    this.inventory = new BehaviorSubject<InventoryItem[]>([]);
  }

  get inventory$(): Observable<InventoryItem[]> {
    return this.inventory.asObservable();
  }

  fetchInventoryItem(): void {

    this.http.get<InventoryItem[]>('/shop/api/inventory')
    .toPromise().then(data => {
      this.inventory.next(data);
    });
  }

  addInventoryItem(item: InventoryItem): void {
    this.http.post<InventoryItem>('shop/api/inventory', item).pipe(
      take(1),
      tap(() => this.fetchInventoryItem())
    ).subscribe();
  }
}

