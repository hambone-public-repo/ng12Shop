import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'inventoryImage'
})
export class InventoryImagePipe implements PipeTransform {

  private defaultImageUrl = 'https://media.istockphoto.com/photos/young-boy-businessman-wears-sad-face-picture-id474749946'

  transform(value: string | undefined | null): string {
    return (value === undefined || value === null) ? this.defaultImageUrl : value;
  }

}
