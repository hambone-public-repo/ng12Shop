
export interface InventoryItem {
    itemName: string;
    itemCost: number;
    itemDesc?: string;
    itemImage?: string;
    cartItemUuid?: string;
}
