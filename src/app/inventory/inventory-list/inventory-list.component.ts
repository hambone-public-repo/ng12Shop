import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { InventoryService } from 'src/app/services/inventory.service';
import { InventoryItem } from '../models/inventory-item.model';

@Component({
  selector: 'ngshop-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  availableTeslaInventory: InventoryItem[] = [];

  constructor(
    private inventoryService: InventoryService,
    private cartService: CartService
  ) { }

  ngOnInit(): void {

    this.inventoryService.inventory$.subscribe(teslaList => {
      console.log('tesla state', teslaList)
      this.availableTeslaInventory = teslaList
    });

    this.inventoryService.fetchInventoryItem();

  }

  onInventoryEvent($event: InventoryItem): void {
    console.log('onInventoryItemEvent', $event);
    this.cartService.addToCart($event);

  }

}


