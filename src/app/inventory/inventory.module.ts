import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AddItemFormComponent } from './add-item-form/add-item-form.component';
import { InventoryItemComponent } from './inventory-item/inventory-item.component';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryImagePipe } from './pipes/inventory-image.pipe';

@NgModule({
  declarations: [
    InventoryListComponent,
    InventoryItemComponent,
    InventoryImagePipe,
    AddItemFormComponent
  ],
  exports: [
    InventoryListComponent,
    AddItemFormComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ]
})
export class InventoryModule { }
