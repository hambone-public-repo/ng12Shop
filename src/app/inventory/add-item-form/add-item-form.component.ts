import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { InventoryService } from 'src/app/services/inventory.service';
import { InventoryItem } from '../models/inventory-item.model';

@Component({
  selector: 'ngshop-add-item-form',
  templateUrl: './add-item-form.component.html',
  styleUrls: ['./add-item-form.component.scss']
})
export class AddItemFormComponent implements OnInit {

  addItemFormGroup: FormGroup;

  constructor(private inventoryService: InventoryService) { 
    this.addItemFormGroup = new FormGroup({
      itemName: new FormControl('', [Validators.required]),
      itemCost: new FormControl('0.00', [Validators.required, validateNumeric]),
      itemDesc: new FormControl('', [Validators.required]),
      itemImage: new FormControl(''),
    })

  }
  
  ngOnInit(): void {
    
  }

  onAddItemSubmit() : void {
    console.log('submit', this.addItemFormGroup?.value);
    this.inventoryService.addInventoryItem(this.addItemFormGroup?.value as InventoryItem);
  }
}

export function validateNumeric(control: AbstractControl) {
  const v = +control.value;
  return v == null || Number.isNaN(v) ? {valid: false} : null;
}