import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AddItemFormComponent } from './add-item-form.component';
import { InventoryService } from 'src/app/services/inventory.service';

fdescribe('AddItemFormComponent', () => {
  let component: AddItemFormComponent;
  let fixture: ComponentFixture<AddItemFormComponent>;
  let compiled: any;
  let mockService: jasmine.SpyObj<InventoryService>;

  beforeEach(async(() => {
      mockService = jasmine.createSpyObj('InventorySerivce', ['addInventoryItem']);

    TestBed.configureTestingModule({
      declarations: [ AddItemFormComponent ],
      imports: [
        MatInputModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: InventoryService, useValue: mockService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddItemFormComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain the form fields for inventory form', () => {
    expect(compiled.querySelector('form#add-item-form')).toBeTruthy();
    expect(compiled.querySelector('input#item-name')).toBeTruthy();
    expect(compiled.querySelector('input#item-cost')).toBeTruthy();
    expect(compiled.querySelector('textarea#item-desc')).toBeTruthy();
    expect(compiled.querySelector('input#item-image')).toBeTruthy();
    expect(compiled.querySelector('button').disabled).toBeTruthy();
  });

  it('should submit', () => {

    const mockPromise = Promise.resolve({
      itemName: 'Mock Item Name',
      itemCost: 123.32,
      itemDesc: 'desc'
    });

    let element = compiled.querySelector('input#item-name');
    element.value = 'Mock Item Name';
    element.dispatchEvent(new Event('input'));

    element = compiled.querySelector('input#item-cost');
    element.value = 123.32;
    element.dispatchEvent(new Event('input'));

    element = compiled.querySelector('textarea#item-desc');
    element.value = 'Mock Item Desc';
    element.dispatchEvent(new Event('input'));

    fixture.detectChanges();
    
    const button = compiled.querySelector('button');
    expect(button.disabled).toBeFalsy();

    button.click();
    fixture.detectChanges();

    expect(mockService.addInventoryItem).toHaveBeenCalledWith({
      itemName: 'Mock Item Name',
      //@ts-ignore
      itemCost: '123.32',
      itemDesc: 'Mock Item Desc',
      itemImage: ''
    });
  })

});
