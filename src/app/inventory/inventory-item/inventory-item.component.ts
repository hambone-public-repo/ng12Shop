import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { InventoryItem } from '../models/inventory-item.model';

@Component({
  selector: 'ngshop-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent implements OnInit {

  @Input() item: InventoryItem | undefined;

  @Output() addItemEvent: EventEmitter<InventoryItem> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onItemAdd(): void {
    console.log('I was clicked');
    this.addItemEvent.emit(this.item);
  }
}