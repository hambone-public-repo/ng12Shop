import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartViewComponent } from './cart/cart-view/cart-view.component';
import { AddItemFormComponent } from './inventory/add-item-form/add-item-form.component';
import { InventoryListComponent } from './inventory/inventory-list/inventory-list.component';

const routes: Routes = [
  { path: 'inventory', component: InventoryListComponent },
  { path: 'cart', component: CartViewComponent },
  { path: 'add', component: AddItemFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
