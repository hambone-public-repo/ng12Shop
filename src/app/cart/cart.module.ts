import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { ShopCommonModule } from '../common/common.module';
import { CartFabComponent } from './cart-fab/cart-fab.component';
import { CartViewComponent } from './cart-view/cart-view.component';

@NgModule({
  declarations: [
    CartFabComponent,
    CartViewComponent
  ],
  exports: [
    CartFabComponent,
    CartViewComponent
  ],
  imports: [
    CommonModule,
    MatBadgeModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    ShopCommonModule
  ]
})
export class CartModule { }
