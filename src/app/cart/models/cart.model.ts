import { InventoryItem } from "src/app/inventory/models/inventory-item.model";

export interface Cart {
    numberOfItems: number,
    total: number,
    items: InventoryItem[];
}
