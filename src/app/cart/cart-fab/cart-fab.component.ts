import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'ngshop-cart-fab',
  templateUrl: './cart-fab.component.html',
  styleUrls: ['./cart-fab.component.scss']
})

export class CartFabComponent implements OnInit, OnDestroy {
  
  subscription: Subscription | undefined;
  
  numberOfItemsInCart = 0;

  constructor(private cartService: CartService) { }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscription = this.cartService.cart$.subscribe(cart => {
      this.numberOfItemsInCart = cart.numberOfItems;
    });
  }

}
