import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from '../models/cart.model';

import { CartViewComponent } from './cart-view.component';

fdescribe('CartViewComponent', () => {
  let component: CartViewComponent;
  let fixture: ComponentFixture<CartViewComponent>;
  let mockCartService: jasmine.SpyObj<CartService>;

  beforeEach(async () => {
    mockCartService = jasmine.createSpyObj('CartService', ['removeFromCart'], ['cart$']);

    await TestBed.configureTestingModule({
      declarations: [CartViewComponent],
      providers: [
        { provide: CartService, useValue: mockCartService }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    
    const mockCart: Cart = {
      numberOfItems: 2,
      total: 3,
      items: [
        {
          itemName: 'mock-name-1',
          itemCost: 1,
          itemDesc: 'mock-desc-1',
          cartItemUuid: 'mock-uuid-1'
        },
        {
          itemName: 'mock-name-2',
          itemCost: 2,
          itemDesc: 'mock-desc-2',
          cartItemUuid: 'mock-uuid-2'
        }
      ]
    };

    const mockCartObservable: Observable<Cart> = of(mockCart);

    //@ts-ignore
    Object.getOwnPropertyDescriptor(mockCartService, 'cart$').get.and.returnValue(
      mockCartObservable
    );

    fixture = TestBed.createComponent(CartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display both cart items', () => {

    const listItemElements: Element[] = fixture.nativeElement.querySelectorAll('mat-list-item')

    expect(listItemElements.length).toEqual(2);
    expect(listItemElements[0].querySelector('.line-name')?.textContent).toContain('mock-name-1')
    
    expect(listItemElements[1].querySelector('.line-name')?.textContent).toContain('mock-name-2')

    expect(
      fixture.nativeElement.querySelector('.cart-view-header>h2').textContent
    )
    .toContain('Number of Current Items: 2');
    
    expect(
      fixture.nativeElement.querySelector('.cart-view-header>h3').textContent
    )
    .toContain('Total Cost of Cart: $3.00');
  });

  it('should execute cart remove first item on click', () => {

    const listItemElement: Element = fixture.nativeElement.querySelector('mat-list-item')

    const icon = listItemElement.querySelector('mat-icon');
    
    (icon as any).click();

    fixture.detectChanges();

    expect(mockCartService.removeFromCart).toHaveBeenCalledTimes(1);

    expect(mockCartService.removeFromCart).toHaveBeenCalledWith({
      itemName: 'mock-name-1',
      itemCost: 1,
      itemDesc: 'mock-desc-1',
      cartItemUuid: 'mock-uuid-1'
    });

  })

  it('should execute cart remove first item on click', () => {

    const listItemElement: Element = fixture.nativeElement.querySelectorAll('mat-list-item')[1];

    const icon = listItemElement.querySelector('mat-icon');
    
    (icon as any).click();

    fixture.detectChanges();

    expect(mockCartService.removeFromCart).toHaveBeenCalledTimes(1);

    expect(mockCartService.removeFromCart).toHaveBeenCalledWith({
      itemName: 'mock-name-2',
      itemCost: 2,
      itemDesc: 'mock-desc-2',
      cartItemUuid: 'mock-uuid-2'
    });

  })
  
});
