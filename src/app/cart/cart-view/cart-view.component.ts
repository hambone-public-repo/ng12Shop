import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { InventoryItem } from 'src/app/inventory/models/inventory-item.model';
import { CartService } from 'src/app/services/cart.service';
import { Cart } from '../models/cart.model';

@Component({
  selector: 'ngshop-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.scss']
})
export class CartViewComponent implements OnInit {

  cart$: Observable<Cart> | undefined;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.cart$ = this.cartService.cart$;
  }

  onDelete(item: InventoryItem): void {
    console.log('delete event', item);
    this.cartService.removeFromCart(item);
  }

}
